#!../../bin/linux-x86_64/vctrl

## You may have to change vctrl to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/vctrl.dbd"
vctrl_registerRecordDeviceDriver pdbbase

epicsEnvSet("STREAM_PROTOCOL_PATH", ".")
## test
#epicsEnvSet("DEVIP", "127.0.0.1")
#epicsEnvSet("DEVPORT", "50123")
#epicsEnvSet("IOCBL", "LPQ")
#epicsEnvSet("IOCDEV", "laseratt")

drvAsynIPPortConfigure("L1","$(DEVIP):$(DEVPORT)",0,0,0)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asyn,PORT=L1,ADDR=0,IMAX=256,OMAX=256")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("laseratt.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=L1")

dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=1")
dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=2")
dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=3")
dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=4")
dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=5")
dbLoadRecords("filter.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=6")

dbLoadRecords("led.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=1")
dbLoadRecords("led.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1,N=2")

dbLoadRecords("protdebug.db","BL=${IOCBL},DEV=${IOCDEV},ASYNPORT=L1")

## autosave
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL), DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
